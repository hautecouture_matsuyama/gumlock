﻿using MiniJSON;
using System;
using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public class VersionUpCheckerScript : MonoBehaviour
{
    //アクセスするスプレッドシート
    //※スクリプトエディタの公開からURLを取得する事
    [SerializeField]
    string URL = "https://script.google.com/macros/s/AKfycbyoBiDsC2yqGpFt4JE3rw25Y89YQN1VeH31ofSR3AOnf1ZNOUs/exec";

    //シートのアプリ名を判定する名前
    [SerializeField]
    string appName;

    //表示するオブジェクト
    [SerializeField]
    GameObject panelObject;

    //シートからのデータを保存しておくスクリプタブルオブジェクト
    [SerializeField]
    Data data;

    //シート側のタブ名を取得する変数
    string _sheetName = "";
    string[] str = new string[2];

    /*[SerializeField]
    Account account;*/

    bool flg = false;

    public static bool isOnece = false;


    void Awake()
    {
        //プラットフォーム変更時に確認すること！！
//URLより読み込みスプレッドシートのシートタブ名を選択（シート側タブは複数可能）
#if UNITY_ANDROID
        _sheetName = "Android";
#elif UNITY_IPHONE
        _sheetName = "IOS";
#endif
    }

    void Start()
    {
       
            //ゲーム起動時にネットワークの接続状況を確認しキャリア・Wifi時は確認処理を行う
            if (Application.internetReachability != NetworkReachability.NotReachable)
            {
                Debug.Log("ネットワークに接続できました。");
                StartCoroutine(Run());
            }
            else
            {
                Debug.Log("ネットワークに接続できません");

            }
        
 
    }

    //シート読み込み検知処理
    IEnumerator Run()
    {
        //読み込先のURLとシートのタブ名を選択
        var download = new WWW(URL + "?sheetName=" + _sheetName);
       
        //読み込みが完了するまで待機
        while (!download.isDone)
        {
            yield return new WaitForSeconds(0.1f);
        }
        Debug.Log(download.text);

        //jsonに値が正常にはいれば処理を行う
        var json = (List<object>)Json.Deserialize(download.text);
        if (json != null)
        {
            var scenario = data;
            scenario.hideFlags = HideFlags.NotEditable;
            scenario.texts = json.Select(j => j.ToString()).ToArray();
          

            //バージョンの判定処理
            for (int i = 0; i < data.texts.Length; i++)
            {
                if (data.texts[i].Contains(appName))
                {
                   
                    if (data.texts[i].Contains(":") || data.texts[i].Contains("："))
                    {
                        str = data.texts[i].Split(new string[] { ":", "：" }, System.StringSplitOptions.None);

                        if (Application.version != str[1])
                        {
                            flg = true;
                            Debug.Log("本アプリのバージョン：" + Application.version);
                            Debug.Log("現在のプラットフォーム:" + _sheetName + "\n" + "シート側の" + str[0] + "のバージョンは" + str[1] + "になります。");
                            Debug.Log("シートとアプリのバージョン差異を検知しました。");
                            panelObject.SetActive(true);
                            AdPosterManager.Instance.AdPosterView(AdPosterManager.AdType.None);
                      //      account.Init();
                        }
                        else
                        {
                        //    account.Init();
                            Debug.Log("This App is New Version");
                            if (i == data.texts.Length && !flg)
                            {
                              //  AdPosterManager.Instance.AdPosterView(AdPosterManager.AdType.Title);
                                Debug.Log("Not Found");
                              //  account.Init();
                            }
                        }
                    }
                
                }
            }
            
        }
        else
        {
            Debug.LogError(download.text);
        }
       
    }


  public void IsOnece(bool flg)
    {
        isOnece = flg;
    }

 
}
