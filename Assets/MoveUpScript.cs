﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class MoveUpScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void StartPop()
    {
        DOTween.To(() => transform.localPosition,
            y => transform.localPosition = y,
            new Vector3(-62f, -345f, 0), 1.2f);
    }
}
