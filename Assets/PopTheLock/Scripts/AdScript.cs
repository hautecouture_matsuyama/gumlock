﻿using UnityEngine;
using System.Collections;
using GoogleMobileAds.Api;

public class AdScript : MonoBehaviour
{

    BannerView bannerView;
    AdRequest request;
    private static bool created = false;

    [SerializeField]
    string androidUnitID = "";
    [SerializeField]
    string iOSUnitID = "";

    public enum Size
    {
        Banner = 0,
        RecBanner
    }

    public enum Positions
    {
        Bottom = 0,
        Top,
    }

    public Size adSize;
    public Positions adPos;



    void Awake()
    {
       /* if (!created)
        {
            // this is the first instance -make it persist
            DontDestroyOnLoad(this.gameObject);
            created = true;
        }
        else
        {
            // this must be aduplicate from a scene reload  - DESTROY!
            Destroy(this.gameObject);
        }*/
        RequestBanner();
    }


    // Use this for initialization
    void Start()
    {
       
    }

    private void RequestBanner()
    {
#if UNITY_ANDROID
       // string adUnitId = "ca-app-pub-2286170402581126/3954559297";
        string adUnitId = androidUnitID;
#elif UNITY_IPHONE
            string adUnitId = iOSUnitID;
#else
            string adUnitId = "unexpected_platform";
#endif

        // Create a 320x50 banner at the top of the screen.
        bannerView = new BannerView(adUnitId, SetSizeBanner(), SetPositionBanner());
        // Create an empty ad request.
         request = new AdRequest.Builder().Build();
        // Load the banner with the request.
        bannerView.LoadAd(request);
        bannerView.Hide();
    }

    AdSize SetSizeBanner()
    {
        if (adSize == Size.Banner)
        {
            return AdSize.Banner;
            
        }else if(adSize == Size.RecBanner){
            return AdSize.MediumRectangle;
            
        }
        return AdSize.Banner;
    }

    AdPosition SetPositionBanner()
    {
        if (adPos == Positions.Bottom)
        {
            return AdPosition.Bottom;
        }
        else if (adPos == Positions.Top)
        {
            return AdPosition.Top;
        }
        return AdPosition.Bottom;
    }




    public void Show()
    {
        bannerView.Show();
    }

    public void Hide()
    {
        bannerView.Hide();
        RequestBanner();
    }

}