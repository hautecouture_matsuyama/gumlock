﻿using UnityEngine;
using System.Collections;

public class OtherURL : MonoBehaviour {
    [SerializeField]
    string androidurl;
    [SerializeField]
    string iosurl;

    string url;

    void Start()
    {
#if UNITY_ANDROID
        url = androidurl;
#elif UNITY_IPHONE
        url = iosurl;
#endif
    }

    public void OpenOtherURL()
    {

        if (Application.internetReachability != NetworkReachability.NotReachable)
        {
            Application.OpenURL(url);
        }
 
    }
}
