﻿using UnityEngine;
using System.Collections;
using NendUnityPlugin.AD;

public class AdPosterManager : MonoBehaviour {

    //シングルトン
    private static AdPosterManager _instance = null;

    public static AdPosterManager Instance
    {
        get
        {
            return _instance;
        }
    }

    public enum AdType
    {
        Title = 0,
        Game,
        GameOver,
        Ranking,
        None,
        banner
    }

    public AdType adType;
    [SerializeField]
    DFPBanner dfpBanner;
    [SerializeField]
    AdScript adBanner;
    [SerializeField]
    AdScript adRecBanner;
    [SerializeField]
    NendAdBanner nendBanner;
    [SerializeField]
    NendAdBanner nendRecBanner;

#if UNITY_ANDROID
    [SerializeField]
    NendAdIcon nendTitleIcon;
    [SerializeField]
    NendAdIcon nendGameOverIcon;
#endif


    private void Awake()
    {
        if (_instance == null)
        {

            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

    }


    public void AdPosterView(AdType adType)
    {
        if (DebugManager.Instance.showBanner)
        {
            switch (adType)
            {
                case AdType.Title:
                    dfpBanner.Show();
                    nendBanner.Show();
                    ShowNendTitleIcon(true);
                    break;
                case AdType.Game:
                    dfpBanner.Hide();
                    nendBanner.Hide();
                    ShowNendTitleIcon(false);
                    break;
                case AdType.GameOver:
                    dfpBanner.Show();
                    nendRecBanner.Show();
                    ShowNendOverIcon(true);
                    break;
                case AdType.Ranking:
                    dfpBanner.Hide();
                    nendRecBanner.Hide();
                    ShowNendOverIcon(false);
                    break;
                case AdType.None:
                    dfpBanner.Hide();
                    nendBanner.Hide();
                    ShowNendTitleIcon(false);
                    nendRecBanner.Hide();
                    ShowNendOverIcon(false);
                    break;
                case AdType.banner:
                    dfpBanner.Show();
                    nendBanner.Show();
                    ShowNendTitleIcon(false);
                    nendRecBanner.Hide();
                    ShowNendOverIcon(false);
                    break;
            }
        }
        else
        {
            dfpBanner.Hide();
            nendBanner.Hide();
            ShowNendTitleIcon(false);
            nendRecBanner.Hide();
            ShowNendOverIcon(false);
        }
    }


    public void ShowNendTitleIcon(bool isShow)
    {
#if UNITY_ANDROID
        if (isShow)
             nendTitleIcon.Show();
        else
             nendTitleIcon.Hide();
#endif
    }

    public void ShowNendOverIcon(bool isShow)
    {
#if UNITY_ANDROID
        if (isShow)
             nendGameOverIcon.Show();
        else
             nendGameOverIcon.Hide();
#endif
    }
}
