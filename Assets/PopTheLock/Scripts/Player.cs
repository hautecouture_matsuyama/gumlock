﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviourHelper
{
    [SerializeField]
    DotPlayerManager dotManager;
    [SerializeField]
    GameObject dotTransform;
    [SerializeField]
    Animator playerAnime;
    [SerializeField]
    GameObject infoObject;
    

	public float speedStart = 5f;

	public bool firstMove;

    public Animator anime;

	float time = 3.7f;

	float direction = 1;

    float rotationSpeed = 1;

    float oldDir = 0;

    bool isOnce= false;

	Vector2 currentDotPosition
	{
		get 
		{
			return dotPosition.GetPosition ();
		}
	}

	[SerializeField] private Transform playerTransform;

	public Transform GetTransform()
	{
		return playerTransform;
	}

	public float GetRotation()
	{
		return transform.eulerAngles.z;
	}

    void Start()
    {
        if (GameManager.isShow) rotationSpeed = DebugManager.Instance.debugIncentiveSpeed;
        Debug.Log("PlayerSpeed:"+rotationSpeed);
    }

	void Awake()
	{
		firstMove = true;
		direction = 1;
	}

	void Update()
	{
        if (!GameManager.isShow)
        {
            rotationSpeed = DebugManager.Instance.debugSpeed;
        }
        else
        {
            rotationSpeed = DebugManager.Instance.debugIncentiveSpeed;
            
        }
       // Debug.Log("NowSpeed:" + rotationSpeed);
		if (!gameManager.gameIsStarted || gameManager.isGameOver || gameManager.isSuccess) 
		{
			firstMove = true;
			StopAllCoroutines ();
			return;
		}

		if (setting.isShowSetting)
			return;

        if (isOnce)
        {
            if (Input.GetMouseButtonDown(0) && !gameManager.isGameOver && !gameManager.isSuccess)
            {
                if (!firstMove && !gameManager.isGameOver) playerAnime.SetTrigger("Shot");
                if (Input.mousePosition.y > Screen.height * 0.9)
                    return;

                //初期移動の場合
                if (firstMove)
                {
                    infoObject.SetActive(false);
                    if (dotPosition.isLeftOfScreen())
                    {
                        direction = -rotationSpeed;
                        Debug.Log("左");
                    }
                    else
                    {
                        direction = +rotationSpeed;
                        Debug.Log("右");
                    }
                    Reset();

                    StartTheMove();

                    firstMove = false;
                    return;
                }

                Vector2 myPos = playerTransform.position;

                float dist = Vector2.Distance(currentDotPosition, myPos);

                //			if (dist <= dotPosition.GetDotSize()) //32
                if (IsSuccess())
                {
                    // dotTransform.GetComponent<RectTransform>().position = new Vector3(0, 434, 180);
                    dotManager.ChangeEnemyImage();
                    gameManager.MoveDone();

                    //dotPosition.GetDotTransform ().localScale = Vector2.zero;

                    if (gameManager.isSuccess)
                        return;

                    dotPosition.DoPosition();

                    StartTheMove();
                }
                else
                {
#if UNITY_EDITOR
                    print("game over from tap before the dot");
#endif
                    gameManager.GameOver();
                }
            }
        }
	}

  public IEnumerator TapWait()
    {
        yield return new WaitForSeconds(0.2f);
        isOnce = true;
    }

    //照準の向きを変更する
	void StartTheMove()
	{
		if (gameManager.isSuccess) 
		{
			StopAllCoroutines ();
			return;
		}
       
        //direction *= -1;

        if (DebugManager.Instance.gameMode)
        {
            direction = dotManager.isPlayerDir();
        }
        else
        {
            direction *= -1;
        }
        
		StopAllCoroutines ();
		StartCoroutine (_StartTheMove());
	}

	float overAngle = 0;
	Vector3 beforeAngle;

	void Reset(){
		dotPlayerManager.UpdateRotations();
		overAngle = 0;
		Debug.Log ("Reset");
	}

	bool IsAfter(){
		return (dotPlayerManager.GetIsAfter (direction) && overAngle > 40f);
//		if (dotPlayerManager.eulerDifference*direction+(Mathf.Sign(direction)>0?0:360) < 0 && Mathf.Abs(dotPlayerManager.eulerDifference) > 40) return true;
	}

    //角度の差で当たり判定を行う
	bool IsSuccess(){
		float angle = Vector3.Angle (dotPosition.transform.up, transform.up);
		return angle <= DebugManager.Instance.debugCollisionAngle;//default=8.276
	}

	IEnumerator _StartTheMove()
	{
//		bool isAfter = false;
        if (!firstMove) yield return new WaitForSeconds(DebugManager.Instance.debugStopTime);
		while (!gameManager.isGameOver) 
		{
			float t0 = transform.rotation.eulerAngles.z;
			float t1 = transform.rotation.eulerAngles.z + direction * 360f;
			float timer = 0;
			Reset();

			while (timer <= time)
			{
				timer += Time.deltaTime;

				float f = Mathf.Lerp (t0, t1, timer / time);

				Vector3 rot = Vector3.forward * f;

				beforeAngle = transform.up;
				transform.eulerAngles = rot;

//				Vector2 myPos = playerTransform.position;

//				Debug.Log (rot);

				if(dotPlayerManager.GetIsAfter (direction)) overAngle += Vector3.Angle(beforeAngle,transform.up);

//				if (!isAfter) 
//				{
//					float dist = Vector2.Distance (currentDotPosition, myPos);
//					if (dist <= dotPosition.GetDotSize()) //10
//					{
//						isAfter = true;
//					}
//				}

				if(IsAfter()){
					#if UNITY_EDITOR
					print ("game over from after the dot");
					#endif
					gameManager.GameOver ();
                    
				}

//				if (isAfter) 
//				{
//					float dist = Vector2.Distance (currentDotPosition, myPos);
//
//					if(dist > dotPosition.GetDotSize()) //20
//					{
//						if (!gameManager.isSuccess) 
//						{
//							#if UNITY_EDITOR
//							print ("game over from after the dot");
//							#endif
//							gameManager.GameOver ();
//						}
//						break;
//					}
//				}

				if (gameManager.isGameOver)
					break;

				yield return null;
			}
		}
	}
}
