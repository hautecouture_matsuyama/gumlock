﻿using UnityEngine;
using System.Collections;
using GoogleMobileAds.Api;

public class AdMobVideo : MonoBehaviour
{


    //シングルトン
    private static AdMobVideo _instance = null;


    public static AdMobVideo Instance
    {
        get
        {
            return _instance;
        }
    }

    public string ios_Reward;
    public string Android_Reward;

    public GameManager gm;

    private RewardBasedVideoAd rewardBasedVideo;

    bool rewardBasedEventHandlersSet = false;

    [SerializeField]
    GameObject moviePanelObject;
    [SerializeField]
    GameObject finishMoviePanelObject;

    // [SerializeField]
    //private TimeManager tm;

    // Use this for initialization
    void Awake()
    {
        if (_instance == null)
        {

            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

    }

    void Start()
    {
        RequestRewardBasedVideo();
    }

    void OnLevelWasLoaded()
    {
        Debug.Log("Scene Was Loaded");
        gm = GameObject.Find("MainCanvas").GetComponent<GameManager>();
        moviePanelObject = gm.moviePanel;
        finishMoviePanelObject = gm.finishMoviePanelObject;
    }

    private void RequestRewardBasedVideo()
    {
        string adUnitId;
#if UNITY_EDITOR
        adUnitId = "unused";
#elif UNITY_ANDROID
        adUnitId = Android_Reward;
#elif UNITY_IPHONE
        adUnitId = ios_Reward;
#else
        adUnitId = "unexpected_platform";
#endif
        rewardBasedVideo = RewardBasedVideoAd.Instance;

        AdRequest request = new AdRequest.Builder().Build();
        rewardBasedVideo.LoadAd(request, adUnitId);

        if (rewardBasedEventHandlersSet == false)
        {
            // has rewarded the user.
            rewardBasedVideo.OnAdRewarded += HandleRewardBasedVideoRewarded;

            rewardBasedVideo.OnAdClosed += HandleRewardBasedVideoRewardedClosed;

            rewardBasedVideo.OnAdFailedToLoad += HandleRewardBasedVideoRewardedFailed;

            rewardBasedEventHandlersSet = true;
        }
    }

    // 動画再生
    public void pushAds()
    {
        if (rewardBasedVideo.IsLoaded())
        {
            rewardBasedVideo.Show();
        }
    }

    // 報酬受け渡し処理
    public void HandleRewardBasedVideoRewarded(object sender, Reward args)
    {
        Debug.Log("最後まで再生した 1");
        moviePanelObject.SetActive(false);
        finishMoviePanelObject.SetActive(true);

        Debug.Log("最後まで再生した 2");
        GameManager.isShow = true;
        PlayerPrefs.SetString("isActiveTimer", "false");

        Debug.Log("最後まで再生した 3");
    }

    // 動画広告リロード
    public void HandleRewardBasedVideoRewardedClosed(object sender, System.EventArgs args)
    {
        RequestRewardBasedVideo();
    }

    // ロード失敗時
    public void HandleRewardBasedVideoRewardedFailed(object sender, AdFailedToLoadEventArgs args)
    {
        StartCoroutine(_waitConnectReward());
    }

    // ロードに失敗した場合、30秒待ってから再ロードをする
    IEnumerator _waitConnectReward()
    {
        while (true)
        {
            yield return new WaitForSeconds(30.0f);

            // 通信ができない場合は、リロードしない
            if (Application.internetReachability != NetworkReachability.NotReachable)
            {
                RequestRewardBasedVideo();
                break;
            }


        }
    }


}
