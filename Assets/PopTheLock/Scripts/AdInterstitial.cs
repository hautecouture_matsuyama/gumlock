﻿using UnityEngine;
using System;
using System.Collections;
using GoogleMobileAds.Api;

public class AdInterstitial : MonoBehaviour {

    private static AdInterstitial _instance = null;

    public static AdInterstitial Instance
    {
        get
        {
            return _instance;
        }
    }

    public string Android_Interstitial;
    public string ios_Interstitial;

    private BannerView bannerView;
    private InterstitialAd _interstitial;

    AdRequest request;

    public string testid;

    public bool testmode;

    bool is_close_interstitial = false;

    // Use this for initialization
    void Awake()
    {
        if (_instance == null)
        {

            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    // Use this for initialization
    void Start()
    {
        // 起動時にインタースティシャル広告をロードしておく
        RequestInterstitial();
        _interstitial.OnAdClosed += delegate(object sender, EventArgs args)
        {
            _interstitial.Destroy();
            RequestInterstitial();
        };
    }
    public void RequestInterstitial()
    {
#if UNITY_ANDROID
        string adUnitId = Android_Interstitial;
#elif UNITY_IPHONE
		string adUnitId = ios_Interstitial;
#else
		string adUnitId = "unexpected_platform";
#endif

        if (is_close_interstitial == true)
        {
            _interstitial.Destroy();
        }

        // Initialize an InterstitialAd.
        _interstitial = new InterstitialAd(adUnitId);
        // Create an empty ad request.
        if (testmode)
        {
            request = new AdRequest.Builder().AddTestDevice(testid).Build();
        }
        else
        {
            request = new AdRequest.Builder().Build();
        }
        // Load the interstitial with the request.
        _interstitial.LoadAd(request);

        is_close_interstitial = false;
    }

    public void ShowInterStitial()
    {
        _interstitial.Show();
    }

    void HandleAdClosed(object sender, System.EventArgs e)
    {
        is_close_interstitial = true;
    }
}
