﻿using UnityEngine;
using System.Collections;

public static class StringExtensions
{
    /// <summary>
    /// 文字列内の改行文字を削除した文字列を返します
    /// </summary>
    public static string RemoveNewLine(this string self)
    {
        return self.Replace("\r", "").Replace("\n", "");
    }
}