﻿using UnityEngine;
using System.Collections;

public class HowToScript : MonoBehaviour {

	[SerializeField]
	GameObject[] pageObs = new GameObject[1];

	[SerializeField]
	GameObject nextBtn,backBtn;

	[SerializeField]
	int pageNo=0;
	
	// Use this for initialization
	void Start () {
		backBtn.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnClickNext(){
		//全ページ数より現在のPage数が小さければ
		if (pageNo<pageObs.Length) {
			//ページ数を加算
			pageNo++;
			if(pageNo >0)backBtn.SetActive(true);
			if(pageNo+1 == pageObs.Length)nextBtn.SetActive(false);
			pageObs [pageNo].SetActive (true);
			pageObs [pageNo-1].SetActive (false);
		}
	}

	public void OnClickBack(){
		//0Page（１ページ）より現在のPage数が大きければ
		if (pageNo>0) {
			//ページ数を加算
			pageNo--;
			if(pageNo <pageObs.Length)nextBtn.SetActive(true);
			if(pageNo == 0)backBtn.SetActive(false);
			pageObs [pageNo].SetActive (true);
			pageObs [pageNo+1].SetActive (false);
		}
	}

	public void OnClickClose(){
        AdPosterManager.Instance.AdPosterView(AdPosterManager.AdType.Title);
		pageNo = 0;
		for (int i = 0; i<pageObs.Length; i++) {
			pageObs[i].SetActive(false);
		}

		nextBtn.SetActive(true);
		backBtn.SetActive(false);
		pageObs[0].SetActive(true);
        gameObject.transform.Find("GameObject").gameObject.SetActive(false);
	}
	public void OnClickOpen(){
        AdPosterManager.Instance.AdPosterView(AdPosterManager.AdType.banner);
		pageObs [0].SetActive (true);
        gameObject.transform.Find("GameObject").gameObject.SetActive(true);
	}
}
