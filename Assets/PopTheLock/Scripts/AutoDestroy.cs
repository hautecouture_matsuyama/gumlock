﻿using UnityEngine;
using System.Collections;

public class AutoDestroy : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Invoke("DestroyEvent",0.5f);
	}

    void DestroyEvent()
    {
        GameObject.Destroy(this.gameObject);
    }

   

}
