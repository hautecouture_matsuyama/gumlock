﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;
using NendUnityPlugin.AD;


public class GameManager : MonoBehaviourHelper
{

	public int NumberOfWinAndLoseToShowAds = 5;
		
	public bool RESET_PLAYER_PREF = false;

	public bool isGameOver = false;
    public static bool isNotFirst = false;

	public Text levelCenterScreen;
	public Text levelTopScreen;

	public Transform theGame;

	public RectTransform lockRect;

	public bool gameIsStarted = false;

    public Animator anime;

    public GameObject playerpos;
    public GameObject effectpos;

    public GameObject effect;
    public GameObject shotEffect;


	[SerializeField] private Button buttonPreviousLevel;
	[SerializeField] private Button buttonNextLevel;
    [SerializeField] private GameObject gameOverPanel;
    [SerializeField] private Text scoreText;
    [SerializeField] private Text bestScoreText;
    [SerializeField]
    private Text titleBestScoreText;
    public GameObject moviePanel;
    [SerializeField] private Image soundButton;
    [SerializeField] private Image soundButton2;
    [SerializeField] private Sprite[] buttons = new Sprite[0];
    [SerializeField] private AudioSource[] audioSource;
    [SerializeField]
    Animator playerAnime;
    [SerializeField]
    GameObject overEnemyImage;
    [SerializeField]
    GameObject scorePanel;
    [SerializeField]
    DotPlayerManager dotManager;

   public GameObject finishMoviePanelObject;

    [SerializeField]
    GameObject mainBGM;
    [SerializeField]
    GameObject overBGM;
   
    [SerializeField]
    string url;
	CanvasScaler canvasScaler;
    bool soundFlag = false;
    public static bool isShow = false;
    private float bgmTime = 0;
    public static bool isRestart = false;


	public bool isSuccess
	{
		get 
		{
			bool success = numTotalOfMove <= 0;
            return false;
		}
	}


	void Awake()
	{

        Input.multiTouchEnabled = false;
		if (RESET_PLAYER_PREF)
			PlayerPrefs.DeleteAll ();

		RESET_PLAYER_PREF = false;

		buttonPreviousLevel.onClick.RemoveListener (OnClickedPreviousLevel);

		buttonNextLevel.onClick.RemoveListener (OnClickedNextLevel);

		SetNewGame ();

		//gameIsStarted = false;

		Application.targetFrameRate = 60;
        DialogManager.Instance.SetLabel("Yes", "No", "Close");

	}

    void Start()
    {
        //初回起動時にベストスコア用変数がなければ作成
        if (!PlayerPrefs.HasKey("bestScore"))
        {
            PlayerPrefs.SetInt("bestScore",0);
        }

        titleBestScoreText.text = PlayerPrefs.GetInt("bestScore").ToString();

        //初回起動時にサウンド用変数がなければ作成
        if (!PlayerPrefs.HasKey("SoundFlag"))
        {
            PlayerPrefs.SetInt("SoundFlag",1);
        }

        //ゲーム起動時・リプレイ時の設定
        if (PlayerPrefs.GetInt("SoundFlag")==1)
        {
            for (int i = 0; i < audioSource.Length; i++)
            {
                audioSource[i].enabled = true;
            }
            soundFlag = false;
            soundButton.sprite = buttons[0];
            soundButton2.sprite = buttons[0];
        }
        else if (PlayerPrefs.GetInt("SoundFlag") == 0)
        {
            for (int i = 0; i < audioSource.Length; i++)
            {
                audioSource[i].enabled = false;
            }
            soundFlag = true;
            soundButton.sprite = buttons[1];
            soundButton2.sprite = buttons[1];
        }



        if (isRestart == true)
        {
           gameIsStarted = true;
            
        }
        else
        {
           gameIsStarted = false;   
        }

      
    }

    public void LateUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {

            //
            // YES NO ダイアログ
            DialogManager.Instance.ShowSelectDialog("終了しますか？", (bool result) => { if (result)Application.Quit(); });

        }
    }

    public void ShowAds()
    {
        AdMobVideo.Instance.pushAds();
    }

	public void GameStart()
	{
		gameIsStarted = true;
	}

	void OnClickedPreviousLevel()
	{
		OnClick (false);
	}

	void OnClickedNextLevel()
	{
		OnClick (true);
	}

	void OnClick(bool isNext)
	{
		buttonPreviousLevel.onClick.RemoveListener (OnClickedPreviousLevel);

		buttonNextLevel.onClick.RemoveListener (OnClickedNextLevel);

		int current = GetCurrentLevel ();

		if(isNext)
			current++;
		else
			current--;

		SetCurrentLevel (current);

		StartCoroutine (StartNewLevel ());
	}

	public void SetNewGame()
	{
		buttonPreviousLevel.interactable = HavePreviousLevel ();

		buttonNextLevel.interactable = HaveNextLevel ();

		buttonPreviousLevel.onClick.AddListener (OnClickedPreviousLevel);

		buttonNextLevel.onClick.AddListener (OnClickedNextLevel);

		isGameOver = false;

		//levelCenterScreen.text = GetCurrentLevel ().ToString ();

		levelTopScreen.text = "LEVEL : " + GetCurrentLevel ().ToString ();

		//numTotalOfMove = GetCurrentLevel ();

		//gameIsStarted = false;

		//lockRect.eulerAngles = Vector3.zero;

		player.transform.eulerAngles = Vector3.zero;

		dotPosition.DoPosition ();


	}

	IEnumerator StartNewLevel()
	{

		buttonPreviousLevel.onClick.RemoveListener (OnClickedPreviousLevel);

		buttonNextLevel.onClick.RemoveListener (OnClickedNextLevel);


		SetNewGame ();

		StartCoroutine (ScreenMove.Move (theGame.GetComponent<RectTransform> (), true));

		while (ScreenMove.isMoving) 
		{
			yield return 0;
		}

		//gameIsStarted = true;
	}



	public int numTotalOfMove = 0;

	public void MoveDone()
	{
		numTotalOfMove++;


        soundManager.PlayTouch();
		levelCenterScreen.text = numTotalOfMove.ToString ();

		bool success = true;
       GameObject go = Instantiate(effect, new Vector3(playerpos.transform.position.x, playerpos.transform.position.y, 1), Quaternion.identity)as GameObject;
       GameObject go2 = Instantiate(shotEffect, new Vector3(effectpos.transform.position.x, effectpos.transform.position.y, 1), Quaternion.identity) as GameObject;
        if (success)
        {
           
            //最大ステージ数に達したら
         /*   if (GetCurrentLevel() != 30)
            {
               
            gameIsStarted = true;            
            LevelCleared();
            }else{
                VSSHARE.DOTakeScreenShot();
                gameIsStarted = false;
                LevelCleared();
            }*/
        }
        else
        {
            buttonstart.isNotFirst = false; 
            soundManager.PlayTouch();
        }
	}

	public void GameOver()
	{
        dotManager.HideEnemyAnimation();
       
        //各種乱数確認
        Debug.Log("Rage:"+UnityEngine.Random.Range(0f, 1f));
        Debug.Log("vale:" + Random.value);
       
       
        if (Random.value < 0.33f)
        {
            if (DebugManager.Instance.showBanner) AdInterstitial.Instance.ShowInterStitial();
        }
        
		soundManager.PlayFail ();
		isGameOver = true;
		StopAllCoroutines ();



		StartCoroutine (_GameOver ());


	}

	IEnumerator _GameOver()
	{
        
        yield return new WaitForSeconds(0.3f);
        anime.SetTrigger("Up");
        playerAnime.SetTrigger("GameOver");
        yield return new WaitForSeconds(1);
        gameOverPanel.SetActive(true);

        mainBGM.SetActive(false);
        overBGM.SetActive(true);

        scorePanel.SetActive(false);
       isShow = false;
        //カウントダウンが終了したら
       if (CountDownEvent.isFinish)
       {
           //33％以下なら動画ポップアップを表示
           if (UnityEngine.Random.Range(0f, 1f) < 0.33f)
           {
               ActiveMoviePanel(true);
           }
           else
           {
               PlayerPrefs.SetString("isActiveTimer", "false");
               //カウントダウンが終了したが33％以下出ない場合はリセット・広告表示を行う
               AdPosterManager.Instance.AdPosterView(AdPosterManager.AdType.GameOver);
           }
       }
       else
       {
           //カウントダウンが終了していない場合はそのまま広告を表示
           AdPosterManager.Instance.AdPosterView(AdPosterManager.AdType.GameOver);
       }
        //ゲームオーバーフラグが立っておりタップ回数が0より多い場合、画面シェイク（ゲームオーバー表現）
        //フラグがfalseの場合は最大ステージ数をクリア後シェイクせずに縮小アニメーションしタイトルへ
        //シェイクアニメーションを一時コメントアウト　2017/3/30＿asae
      /*  if (isGameOver && numTotalOfMove >= 0)
        {
            StartCoroutine(ScreenShake.Shake(theGame, 0.10f));
        }*/
       
        //anime.SetTrigger("Backnext");

        //今回のスコアの表示
        scoreText.text = numTotalOfMove.ToString();


        //ベストスコアの更新
        if (numTotalOfMove > PlayerPrefs.GetInt("bestScore"))
        {
            //前回より高得点なのでベストスコアを更新して表示
            PlayerPrefs.SetInt("bestScore", numTotalOfMove);
            bestScoreText.text = PlayerPrefs.GetInt("bestScore").ToString();

            //スコアの送信
            RankingManager.Instance.SendScore(numTotalOfMove);
        }
        else
        {
            //前回スコアより低いのでそのまま表示
            bestScoreText.text = PlayerPrefs.GetInt("bestScore").ToString();
        }

        RankingManager.Instance.GetRankingData();

        yield return new WaitForSeconds(0.5f);
        //AdPosterManager.Instance.AdPosterView(AdPosterManager.AdType.GameOver);
        
		while (ScreenShake.isShaking) 
		{
			yield return 0;
		}
		/*#if UNITY_5_3
		SceneManager.LoadScene (0);
		#else
		Application.LoadLevel(Application.loadedLevel);
		#endif*/
	}

	public void LevelCleared()
	{
		#if APPADVISORY_ADS
		if(PlayerPrefs.GetInt("GAMEOVER_COUNT",0) > NumberOfWinAndLoseToShowAds && AdsManager.instance.IsReadyInterstitial())
		{
			PlayerPrefs.SetInt("GAMEOVER_COUNT",0);
			AdsManager.instance.ShowInterstitial();
		}
		else
		{
			PlayerPrefs.SetInt("GAMEOVER_COUNT",PlayerPrefs.GetInt("GAMEOVER_COUNT",0)  + 1);
		}
		PlayerPrefs.Save();
		#endif

		soundManager.PlaySuccess ();


		int current = GetCurrentLevel ();

        if (current != 30)
        {
            current++;


            SetCurrentLevel(current);

            SetMaxLevel(current);
        }
		StartCoroutine (_LevelCleared ());

//		if (current%3 == 0)
			colorManager.ChangeColor();
	}

	IEnumerator _LevelCleared()
	{
		float t0 = 0f;
		float t1 = -30f;
		float timer = 0;
		float time = 0.5f;
		while (timer <= time) {
			timer += Time.deltaTime;

			float f = Mathf.Lerp (t0, t1, timer / time);

			Vector3 rot = Vector3.forward * f;

			//lockRect.eulerAngles = rot;

			yield return 0;
		}

        yield return new WaitForSeconds(0.2f);
        //最大ステージ数に達しておりクリアした場合はスケールアニメーション後タイトルへ
        

     /*   //最大ステージ数未満でかつステージをクリアした場合はスライドアニメーション
        if (GetCurrentLevel() != 30 && numTotalOfMove <= 0)
        {


            StartCoroutine(ScreenMove.Move(theGame.GetComponent<RectTransform>(), false));

            while (ScreenMove.isMoving)
            {
                yield return 0;
            }

            //		SceneManager.LoadScene (0);

            StartCoroutine(StartNewLevel());
        }
        else if (GetCurrentLevel() == 30 && numTotalOfMove <= 0)
        {
            StartCoroutine(_GameOver());
        }*/
	}

	public int GetMaxLevel()
	{
		int max = PlayerPrefs.GetInt ("MAX_LEVEL", 1);

		return max;
	}


	public void SetMaxLevel(int level)
	{
		int max = GetMaxLevel ();

		if (max < level) 
		{
			PlayerPrefs.SetInt ("MAX_LEVEL", level);
			PlayerPrefs.Save ();
		}
	}


	public int GetCurrentLevel()
	{
		int current = PlayerPrefs.GetInt ("CURRENT_LEVEL", 1);

		if (current <= 0) 
		{
			current = 1;
			PlayerPrefs.SetInt ("CURRENT_LEVEL", 1);
			PlayerPrefs.Save ();
		}

		return current;
	}


	public void SetCurrentLevel(int level)
	{
		PlayerPrefs.SetInt ("CURRENT_LEVEL", level);
		PlayerPrefs.Save ();
	}


	public bool HavePreviousLevel()
	{
		int currentLevel = GetCurrentLevel ();

		if (currentLevel > 1)
			return true;

		return false;
	}

	public bool HaveNextLevel()
	{
		int currentLevel = GetCurrentLevel ();

		int maxLevel = GetMaxLevel ();

		if (currentLevel < maxLevel)
			return true;

		return false;
	}

    public void ReStartGame()
    {
        if (CountDownEvent.isFinish) CountDownEvent.isFinish = false;
        AdPosterManager.Instance.AdPosterView(AdPosterManager.AdType.None);
        isRestart = true;
        #if UNITY_5_3
		SceneManager.LoadScene (0);
		#else
		Application.LoadLevel(Application.loadedLevel);
		#endif
    }

    public void ReStart()
    {
        if (CountDownEvent.isFinish) CountDownEvent.isFinish = false;
        AdPosterManager.Instance.AdPosterView(AdPosterManager.AdType.None);
        isRestart = false;
#if UNITY_5_3
		SceneManager.LoadScene (0);
#else
        Application.LoadLevel(Application.loadedLevel);
#endif
    }

    public void OpenRankingPanel()
    {
        AdPosterManager.Instance.AdPosterView(AdPosterManager.AdType.Ranking);
        AdPosterManager.Instance.AdPosterView(AdPosterManager.AdType.banner);
        RankingManager.Instance.ShowRanking();

    }

    public void ActiveMoviePanel(bool flag)
    {
        if (flag)
        {
            AdPosterManager.Instance.AdPosterView(AdPosterManager.AdType.None);
        }
        else
        {
            PlayerPrefs.SetString("isActiveTimer", "false");
            AdPosterManager.Instance.AdPosterView(AdPosterManager.AdType.GameOver);
        }
        moviePanel.SetActive(flag);
    }

    public void SoundEnableButton()
    {
        soundFlag = !soundFlag;
        if (soundFlag)
        {
            PlayerPrefs.SetInt("SoundFlag",0);//オン
            for (int i = 0; i < audioSource.Length; i++)
            {
                audioSource[i].enabled = false;
            }
            soundButton.sprite = buttons[1];
            soundButton2.sprite = buttons[1];
        }
        else
        {
            PlayerPrefs.SetInt("SoundFlag", 1);//オフ
            for (int i = 0; i < audioSource.Length; i++)
            {
                audioSource[i].enabled = true;
            }
            soundButton.sprite = buttons[0];
            soundButton2.sprite = buttons[0];
        }

    }

    public void StoreOpenURL()
    {
        Application.OpenURL(url);
    }

}
