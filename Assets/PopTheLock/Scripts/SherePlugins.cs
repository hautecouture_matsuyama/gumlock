﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SherePlugins : MonoBehaviour {

    public Texture2D shareTexture;

    [SerializeField]
    string androidURL="";
    [SerializeField]
    string iosURL = "";

    GameManager gameManager;

    string storeURL = "";

    void Start()
    {
        gameManager = GameObject.Find("MainCanvas").GetComponent<GameManager>();
#if UNITY_ANDROID
        storeURL = androidURL;
#elif UNITY_IPHONE
        storeURL = iosURL;
#endif
    }

    public void ShareTwitter()
    {

        int score = 0;
		string url = storeURL;
        UM_ShareUtility.TwitterShare("【撃退！モンスターバスター】 " + gameManager.numTotalOfMove + " 体倒したよ！君はどれだけ倒せる？" + url+"#おかしなガムボール", shareTexture);
		//Debug.Log ("Pushed!");
    }

    public void ShareLINE()
    {
        int score = 0;
        string msg = "【撃退！モンスターバスター】 " + gameManager.numTotalOfMove + " 体倒したよ！君はどれだけ倒せる？" + storeURL;
        string url = "https://line.me/R/msg/text/?" + WWW.EscapeURL(msg);
        Application.OpenURL(url);
    }

 

}