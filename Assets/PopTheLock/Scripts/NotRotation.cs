﻿using UnityEngine;
using System.Collections;

public class NotRotation : MonoBehaviour {
    public GameObject objTarget;

    void LateUpdate()
    {
        gameObject.GetComponent<RectTransform>().position = objTarget.GetComponent<RectTransform>().position;
    }

}
