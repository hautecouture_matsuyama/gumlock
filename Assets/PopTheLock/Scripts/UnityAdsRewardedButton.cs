﻿/*using UnityEngine;
using System.Collections;
using UnityEngine.Advertisements;

public class UnityAdsRewardedButton : MonoBehaviour
{

    public string gameId; // Set this value from the inspector.
    public bool enableTestMode = true;
    public GameObject moviePanelObject;
    public GameObject finishMoviePanelObject;

    void Start()
    {
        Advertisement.Initialize(gameId, enableTestMode);
    }
    public void pushBtn()
    {
        if (Advertisement.IsReady())
        {
            Debug.Log("準備ok");

            //ShowOptionsクラスを定義
            ShowOptions showoptions = new ShowOptions();
            // showoptions. = true; //trueだとブロッキングする
            showoptions.resultCallback = resAds;  //コールバック先メソッド

            //広告呼び出し
            Advertisement.Show(null, showoptions);
        }
        else
        {
            Debug.Log("準備ng");
        }
    }

    //コールバック受け取り用メソッド
    public void resAds(ShowResult res)
    {
        if (res == ShowResult.Failed)
        {
            Debug.Log("再生失敗");
        }
        else if (res == ShowResult.Skipped)
        {
            Debug.Log("途中でスキップした");
            GameManager.isShow = true;
            PlayerPrefs.SetString("isActiveTimer", "false");
            moviePanelObject.SetActive(false);
            finishMoviePanelObject.SetActive(true);
            AdPosterManager.Instance.AdPosterView(AdPosterManager.AdType.GameOver);
        }
        else if (res == ShowResult.Finished)
        {
            Debug.Log("最後まで再生した");
            GameManager.isShow = true;
            PlayerPrefs.SetString("isActiveTimer", "false");
            moviePanelObject.SetActive(false);
            finishMoviePanelObject.SetActive(true);
            AdPosterManager.Instance.AdPosterView(AdPosterManager.AdType.GameOver);
        }
    }
}*/