﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;

/*カウントダウンタイマー制御クラス
 * @since  2017-1-19
 * @author Keijiro Asae
 * @copyright (C)2017 hautecouture.inc
 */

public class CountDownEvent : MonoBehaviour {
    [SerializeField]
    Text timer;

    public float gameTimer;
    public int gameSecond;

    public int hours;
    public int minutes;
    public int seconds;

    private  string remainingTime;
    private float timerValue;
    private bool isPause = false;
    private bool isTimer = false;
    public static bool isFinish = false;


    //バックグラウンド移行検知関数
    //移行時の時間を保存、復帰時に現在時刻から移行時の時間の差分を算出しタイマーから引き
    //経過時間の差分を引いた現在カウントを算出する
    void OnApplicationPause(bool pauseStatus)
    {
        if (pauseStatus)
        {
            Debug.Log("applicationWillResignActive or onPause");
            //ポーズ実行中
            isPause = true;
            // 時刻の保存
            System.DateTime now = DateTime.Now;
            PlayerPrefs.SetString("oldDate", now.ToBinary().ToString());

        }
        else
        {
            if (isPause)
            {
                gameTimer = TimeSubtraction("oldDate", gameTimer);
                if (TimeSubtraction("oldDate", gameTimer) <= 0)
                {
                    timer.text = "Get";
                    isTimer = false;
                    //PlayerPrefs.SetString("isActiveTimer", "false");

                }
                //ポーズ終了
                isPause = false;
            }
        }
    }

    
    void Start()
    {

        if (!PlayerPrefs.HasKey("isActiveTimer"))
        {
            PlayerPrefs.SetString("isActiveTimer", "false");
        }

        if (PlayerPrefs.GetString("isActiveTimer") == "false")
        {
            TestjobStart();
        }
        //ゲーム起動時の現在カウント算出メソッド
        TimerStartUp();
    }


    
    void Update()
    {
      //タイマーが起動している時はカウント処理を行う
        if(isTimer)TimerCountEvent();
    }

    /*前回、ゲーム起動時にタイマーを起動させていたら
     * 「タイマー起動時間-現在時刻」の差分から現在カウントを算出する
     */
    void TimerStartUp()
    {
        //前回起動時にタイマーを起動していたら
        if (PlayerPrefs.GetString("isActiveTimer") == "true")
        {
            Debug.Log("Timer is True");
            if (TimeSubtraction("startupDate", gameSecond) > 0)
            {
                //前回からのタイマー起動時間からアプリ起動時間の差分を引いて現在カウント時間にする
                gameTimer = TimeSubtraction("startupDate", gameSecond);
                isTimer = true;
            }
            else if (TimeSubtraction("startupDate", gameSecond) <= 0)
            {
                timer.text = "Get";
                isTimer = false;
                isFinish = true;
            //    PlayerPrefs.SetString("isActiveTimer", "false");
                
            }
        }
    }

    //タイマー処理メソッド
    /*
     * 設定した秒数からhours,minutes,secondsを算出
     * カウントが0になると終了処理を行う
     */
    void TimerCountEvent()
    {
        timerValue += 1 * Time.deltaTime;
        hours = Mathf.FloorToInt((float)(gameTimer - timerValue) / 3600);
        minutes = Mathf.FloorToInt((float)(gameTimer - timerValue - hours * 3600) / 60);
        seconds = Mathf.FloorToInt((gameTimer - timerValue) % 60);

        remainingTime = string.Format("{2:00} : {0:00} : {1:00}", minutes, seconds, hours);

        //カウントが終了したら
        if (seconds == 0 && minutes == 0)
        {
            //値の保存及び初期化処理
            timer.text = "Get";
            isTimer = false;
            isFinish = true;
          //  PlayerPrefs.SetString("isActiveTimer","false");
        }
        else
        {
            //カウント実行中はテキストに算出した時間を文字列で代入
            timer.text = remainingTime.ToString();
        }
    }

    //時間差分算出用メソッド
    //現在時間から指定の時間を引いて経過時間の差分を求める
    float TimeSubtraction(string dataName,float timer)
    {
        // 時刻の読み出し
        string datetimeString = PlayerPrefs.GetString(dataName);
        System.DateTime datetime = System.DateTime.FromBinary(System.Convert.ToInt64(datetimeString));
        TimeSpan ts = DateTime.Now - datetime;   //DateTime の差が TimeSpan として返る
        timer = timer - (float)ts.TotalSeconds;
        Debug.Log(ts.TotalSeconds + "秒の差があります。");
        return timer;
    }

    //仮タイマー実行処理
    public void TestjobStart()
    {
        timerValue = 0;
        gameTimer = gameSecond;
        isTimer = true;
        // タイマースタート時の時刻の保存
        isPause = true;
        System.DateTime now = DateTime.Now;
        PlayerPrefs.SetString("startupDate", now.ToBinary().ToString());
        PlayerPrefs.SetString("isActiveTimer", "true");
    }

}
