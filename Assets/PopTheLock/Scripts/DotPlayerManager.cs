﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DotPlayerManager : MonoBehaviour {

	[SerializeField] RectTransform _playerTransform;
	[SerializeField] RectTransform _dotTransform;

    [SerializeField]
    RectTransform _playerTransform2;
    [SerializeField]
    RectTransform _dotTransform2;

    [SerializeField] Sprite[] enemyImages = new Sprite[0];
    [SerializeField] Sprite[] gameOverEnemyImages = new Sprite[0];
    [SerializeField] Image overEnemyImage;
    [SerializeField] Image DotImage;
    [SerializeField] Animator anime;
    [SerializeField]
    GameObject playerImageObject;
    float speed;

	RectTransform rectTransform{get{return (RectTransform)transform;}}

	public void UpdateRotations(){
		var startPlayerRotation = _playerTransform.rotation;
		var dotRot = _dotTransform.rotation;
		rectTransform.rotation = startPlayerRotation;
		_playerTransform.localRotation = Quaternion.identity;
		_dotTransform.rotation = dotRot;
	}

	public float eulerDifference{get{ return _playerTransform.localEulerAngles.z-_dotTransform.localEulerAngles.z;}}

	public float GetDifference(float direction){
		return eulerDifference*-direction;
	}

	public bool GetIsAfter(float direction){
		//Debug.Log (GetDifference(direction));
		return GetDifference (direction) < 0;
	}

	public float eulerDiffenceAbs{get{return Mathf.Abs (eulerDifference);}}

    public float isPlayerDir()
    {

        if (!GameManager.isShow)
        {
            speed = DebugManager.Instance.debugSpeed;
        }
        else
        {
            speed = DebugManager.Instance.debugIncentiveSpeed;
        }

        if (_playerTransform2.eulerAngles.z < Mathf.DeltaAngle(_dotTransform2.eulerAngles.z, _playerTransform2.eulerAngles.z))
            {

                Debug.Log("A");
                return speed;
            }
        else if (_playerTransform2.eulerAngles.z > Mathf.DeltaAngle(_dotTransform2.eulerAngles.z, _playerTransform2.eulerAngles.z))
            {
                if (Mathf.DeltaAngle(_dotTransform2.eulerAngles.z, _playerTransform2.eulerAngles.z) > 0)
                {
                    Debug.Log("B");
                    return speed;
                }
                else
                {
                    Debug.Log("B");
                    return -speed;
                }
            }
        Debug.Log("Error");
        return speed;
    }


    public void ChangeEnemyImage()
    {
        anime.SetTrigger("PopUp");
        int rand = UnityEngine.Random.Range(0, enemyImages.Length);
        DotImage.sprite = enemyImages[rand];
        overEnemyImage.sprite = gameOverEnemyImages[rand];
        
    }

    public void DotColorAlpha()
    {
        DotImage.color = new Color(1,1,1,0);
    }

    public void HideEnemyAnimation()
    {
        anime.SetTrigger("Hide");
        playerImageObject.SetActive(false);
    }

}
