﻿using UnityEngine;
using System.Collections;

public class OpenHowToScript : MonoBehaviour {

    [SerializeField]
    GameObject panelObject;

    //遊び方パネルを開く
    public void OpenPanel()
    {
        panelObject.SetActive(true);
    }

    //遊び方パネルを閉じる
    public void ClosePanel()
    {
        panelObject.SetActive(false);
    }
}
