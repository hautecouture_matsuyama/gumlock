﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DefaultName : MonoBehaviour
{
    //string[] nameList = new string[1000];
    [SerializeField]
    InputField inputText;
    void Start()
    {
        TextAsset nameTextFile = Resources.Load("text/defaultname", typeof(TextAsset)) as TextAsset;
           string[] nameList = nameTextFile.text.Split("\n"[0]);
        int num=Random.Range(0,nameList.Length);
        inputText.text = nameList[num].RemoveNewLine();
        Debug.Log(nameList[num]);
    }
   

}
