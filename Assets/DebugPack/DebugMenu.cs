﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

[System.Serializable]
    public class Speed
    {
        public float speed;
        public float incentiveSpeed;
        public Image speedButton;
    }

[System.Serializable]
public class CollisionSize
    {
        public Vector2 collisionSize;
        public float collisionAngle;
        public Image collisionButton;
    }

[System.Serializable]
public class StopTime
    {
        public float stopTime;
        public Image stopButton;
    }


[System.Serializable]
public class ShowHideBanner
{
    public Image showButton, hideButton;
    
}

[System.Serializable]
public class ModeImage
{
    public Image normalButton, hardButton;

}

public class DebugMenu : MonoBehaviour {

    [SerializeField]
    Speed[] speed;

    [SerializeField]
    CollisionSize[] collisionsize;

    [SerializeField]
    StopTime[] stopTme;

    [SerializeField]
    ShowHideBanner showhideBanner;

    [SerializeField]
    ModeImage modeImage;

   /* void Awake()
    {
        DebugManager.Instance.debugIncentiveSpeed = (float)speed[0].incentiveSpeed;
    }
    */

    // Use this for initialization
    void Start () {
        
            if(!(DebugManager.Instance.speed_number == 0))
            {
                speed[0].speedButton.color = Color.white;
                speed[DebugManager.Instance.speed_number].speedButton.color = Color.gray;
            }
            if (!(DebugManager.Instance.collision_number == 0))
            {
                collisionsize[0].collisionButton.color = Color.white;
                collisionsize[DebugManager.Instance.collision_number].collisionButton.color = Color.gray;
            }
            if (!(DebugManager.Instance.stop_number == 0))
            {
                stopTme[0].stopButton.color = Color.white;
                stopTme[DebugManager.Instance.stop_number].stopButton.color = Color.gray;
            }
            if (!(DebugManager.Instance.showBanner))
            {
                showhideBanner.showButton.color = Color.white;
                showhideBanner.hideButton.color = Color.gray;
            }
        

        if (!DebugManager.Instance.gameMode)
        {
            modeImage.hardButton.color = Color.white;
            modeImage.normalButton.color = Color.gray;
        }
        else
        {
            modeImage.hardButton.color = Color.gray;
            modeImage.normalButton.color = Color.white;
        }
	
	}
	
	// Update is called once per frame
	void Update () {
	}

    public void SetSpeed(int number)
    {
        for (int i = 0; i < speed.Length; i++)
        {
            if(number == i)
            {
                speed[i].speedButton.color = Color.grey;
            }
            else
            {
                speed[i].speedButton.color = Color.white;
            }
        }
        DebugManager.Instance.debugSpeed = speed[number].speed; ;
        DebugManager.Instance.debugIncentiveSpeed = speed[number].incentiveSpeed;
        DebugManager.Instance.debug = true;
        DebugManager.Instance.speed_number = number;
    }

    public void SetCollisionSize(int number)
    {
        for (int i = 0; i < speed.Length; i++)
        {
            if (number == i)
            {
                collisionsize[i].collisionButton.color = Color.grey;
            }
            else
            {
                collisionsize[i].collisionButton.color = Color.white;
            }
        }
        DebugManager.Instance.debugCollisionSize = collisionsize[number].collisionSize;
        DebugManager.Instance.debugCollisionAngle = collisionsize[number].collisionAngle;
        DebugManager.Instance.debug = true;
        DebugManager.Instance.collision_number = number;
    }

    public void SetStopTime(int number)
    {
        for (int i = 0; i < speed.Length; i++)
        {
            if (number == i)
            {
                stopTme[i].stopButton.color = Color.grey;
            }
            else
            {
                stopTme[i].stopButton.color = Color.white;
            }
        }
        DebugManager.Instance.debugStopTime = stopTme[number].stopTime;
        DebugManager.Instance.debug = true;
        DebugManager.Instance.stop_number = number;
    }

    public void SetShowBanner(int number)
    {
        if(number == 0)
        {
            DebugManager.Instance.showBanner = true;
            showhideBanner.showButton.color = Color.gray;
            showhideBanner.hideButton.color = Color.white;
        }
        else
        {
            DebugManager.Instance.showBanner = false;
            showhideBanner.showButton.color = Color.white;
            showhideBanner.hideButton.color = Color.gray;
        }
    }

    public void SetModeButton(int number){
        if (number == 0)
        {
            DebugManager.Instance.gameMode = true;
            modeImage.hardButton.color = Color.gray;
            modeImage.normalButton.color = Color.white;
        }
        else
        {
            DebugManager.Instance.gameMode = false;
            modeImage.hardButton.color = Color.white;
            modeImage.normalButton.color = Color.gray;
        }
    }

}
