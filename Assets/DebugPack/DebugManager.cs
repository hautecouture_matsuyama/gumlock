﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class DebugManager : MonoBehaviour {

    private static DebugManager mInstace;

    public static DebugManager Instance
    {
        get {
            if (mInstace == null)
                mInstace = new DebugManager();
            return mInstace;
        }

    }

    public float debugSpeed;
    public float debugIncentiveSpeed;
    public Vector2 debugCollisionSize;
    public float debugCollisionAngle;
    public float debugStopTime;

    public bool debug;
    public bool showBanner;
    public bool gameMode = false;

    public int speed_number;
    public int collision_number;
    public int stop_number;

    void Awake()
    {

        if (mInstace == null)
        {

            mInstace = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {

            Destroy(gameObject);
        }

    }

    // Use this for initialization
    void Start()
    {

    }
	
	// Update is called once per frame
	void Update () {
	
	}

}
