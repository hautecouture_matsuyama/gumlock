﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class buttonstart : MonoBehaviour {

    public GameManager gm;
	public GameObject Background;
    public GameObject BGMObject;
    public GameObject ScoreObject;
    public GameObject infoObject;
    public Image playerImage;
    public Player player;
    public Image dotImage;
    public Animator dotanime;
    [SerializeField]
    GameObject mainBGM;
    [SerializeField]
    GameObject gameBGM;
    [SerializeField]
    GameObject overBGM;

    [SerializeField]
    MoveUpScript upPayer;

	public static bool isNotFirst = false;
  //  public Animator anime;

    void Awake()
    {
        if (GameManager.isRestart) START();
    }

    void Start()
	{

      /*  if (isNotFirst == true)
        {
            gm.gameIsStarted = true;
            //Background.SetActive(false);
            //anime.SetTrigger("next");
        }
        else
        {
            if (isNotFirst == false)
            {
                gm.gameIsStarted = false;
            }
        }*/
		
	}
	public void START() 

	{
        AdPosterManager.Instance.AdPosterView(AdPosterManager.AdType.Game);
        player.StartCoroutine("TapWait");
        mainBGM.SetActive(false);
        gameBGM.SetActive(true);
        upPayer.StartPop();
        playerImage.enabled = true;
        ScoreObject.SetActive(true);
        BGMObject.SetActive(true);
        infoObject.SetActive(true);
		 Background.SetActive(false);
         dotImage.enabled = true;
         dotanime.enabled = true;
		//Debug.Log ("START");
	}
}
