﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class SettingButton : MonoBehaviourHelper {
	
	public GameObject SettingPanel;

	public bool isShowSetting;

	private void OpenSetting(){
		isShowSetting = true;
	}
	public void CloseSetting(){
		isShowSetting = false;
	}

	public void Setting () 
	{
		if (!player.firstMove)
			return;
		SettingPanel.SetActive(true);
		OpenSetting();
	}
}
