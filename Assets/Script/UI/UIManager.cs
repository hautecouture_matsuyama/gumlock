﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class UIManager : MonoBehaviour {
	
	[SerializeField] public UIScreen[] Screen;
	private UIScreen currentScreen;

	// Use this for initialization
	void Start () {
		for (int i = 0; i < Screen.Length; i++) {

			Screen [i].First();
			if (i != 0) {
				Screen [i].SetactiveScreen (false);
			}

		
		}
		Screen [0].SetactiveScreen (true);
		currentScreen = Screen [0];
	}

	public void ShowScreen(int ScreenNumber)
	{
		if (currentScreen == Screen [ScreenNumber])
			return;
		currentScreen.SetactiveScreen (false);
		Screen [ScreenNumber].SetactiveScreen (true);
		currentScreen = Screen[ScreenNumber];
	}
}

[System.Serializable]
public class UIScreen{


	public Canvas canvas;
	[System.NonSerialized] public CanvasGroup canvasGroup;
	[System.NonSerialized] public GraphicRaycaster Grahicraycaster;

	public void First ()
	{
		canvasGroup = canvas.GetComponent<CanvasGroup> ();
		Grahicraycaster = canvas.GetComponent<GraphicRaycaster> ();
	}

	public void SetactiveScreen(bool enabled)

	{
		if (enabled) {
			Grahicraycaster.enabled = true;
			canvasGroup.alpha = 1;
			canvas.gameObject.SetActive(true);
		} else
		{
			Grahicraycaster.enabled = false;
			canvasGroup.alpha = 0;
			canvas.gameObject.SetActive(false);			
		}
	}

} 